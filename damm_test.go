package damm

import (
	"math/rand"
	"strconv"
	"testing"
	"time"
)

const numberOfTests = 100

func TestVerify(t *testing.T) {
	if !Verify("5724") {
		t.Error("5724 is valid")
	}
}

func TestVerifyWithLetters(t *testing.T) {
	if Verify("hello") {
		t.Error("submitted string not numbers")
	}
}

func TestCalculateWithLetters(t *testing.T) {
	_, err := CalculateNumberWithCheckDigit("hello")
	if err == nil {
		t.Error("Should have returned error")
	}
}

func TestCalculate(t *testing.T) {
	rand.Seed(time.Now().UTC().UnixNano())

	for i := 0; i < numberOfTests; i++ {
		number := strconv.Itoa(rand.Intn(8999999999) + 1000000000)
		check, err := CalculateNumberWithCheckDigit(number)
		if err != nil {
			t.Error(err)
		}
		if !Verify(check) {
			t.Error("number should have been valid")
		}

	}

}

func TestCalculateWithIncorrectDigits(t *testing.T) {
	rand.Seed(time.Now().UTC().UnixNano())

	for i := 0; i < numberOfTests; i++ {
		number := strconv.Itoa((8999999999) + 1000000000)
		check, err := CalculateCheckDigit(number)
		if err != nil {
			t.Error(err)
		}

		n := strconv.Itoa(rand.Intn(10))
		for n == check {
			n = strconv.Itoa(rand.Intn(10))
		}

		if Verify(number + n) {
			t.Error("number should have been invalid")
		}

	}
}

var res bool

func BenchmarkVerify(b *testing.B) {
	const number = "134532"
	var v bool
	for n := 0; n < b.N; n++ {
		v = Verify(number)
	}
	res = v
}
func BenchmarkCalculate(b *testing.B) {
	const number = "134532"
	var v bool
	for n := 0; n < b.N; n++ {
		r, _ := CalculateNumberWithCheckDigit(number)
		v = r == ""
	}
	res = v

}
