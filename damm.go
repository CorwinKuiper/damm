package damm

// Check digit calculation using the Damm algorithm https://en.wikipedia.org/wiki/Damm_algorithm

import (
	"errors"
)

// ErrNotANumber is when the string contains non number characters
var ErrNotANumber = errors.New("Not a number")

var dammGrid = [10][10]uint8{
	{0, 3, 1, 7, 5, 9, 8, 6, 4, 2},
	{7, 0, 9, 2, 1, 5, 4, 8, 6, 3},
	{4, 2, 0, 6, 8, 7, 1, 3, 5, 9},
	{1, 7, 5, 0, 9, 8, 3, 4, 2, 6},
	{6, 1, 2, 3, 0, 4, 5, 9, 7, 8},
	{3, 6, 7, 4, 2, 0, 9, 5, 8, 1},
	{5, 8, 6, 9, 7, 2, 0, 1, 3, 4},
	{8, 9, 4, 5, 3, 6, 2, 0, 1, 7},
	{9, 4, 3, 8, 6, 1, 7, 2, 0, 5},
	{2, 5, 8, 1, 4, 3, 6, 7, 9, 0},
}

// Verify returns true if the number has the correct check digit
// and returns false if the number has an incorrect check digit
// or if the number contains non numeric digits.
func Verify(intString string) bool {
	if !isNumber(intString) {
		return false
	}

	row := uint8(0)
	for _, n := range intString {
		number := int(n) - '0'
		row = dammGrid[row][number]
	}

	return row == 0

}

// CalculateNumberWithCheckDigit returns the number with the added checkdigit
func CalculateNumberWithCheckDigit(number string) (string, error) {
	digit, err := CalculateCheckDigit(number)
	if err != nil {
		return "", err
	}
	return number + digit, nil
}

// CalculateCheckDigit returns the check digit for the number
func CalculateCheckDigit(number string) (string, error) {

	if !isNumber(number) {
		return "", ErrNotANumber
	}

	row := uint8(0)

	for _, r := range number {
		n := int(r) - '0'
		row = dammGrid[row][n]
	}

	digit := string(row + '0')

	return digit, nil

}

func isNumber(intString string) bool {
	for _, r := range intString {
		if !(r >= '0' && r <= '9') {
			return false
		}
	}
	return true
}
